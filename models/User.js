const Data = require("../models/Model");

class User extends Data {
  constructor() {
    super();
  }

  getAllUser() {
    return super.getAllDatas("users");
  }

  getUserById(id) {
    return super.getDataById(id, "users");
  }
}

module.exports = User;
