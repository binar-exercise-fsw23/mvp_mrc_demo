class Model {
  constructor() {
    this.posts = require("../db/posts.json");
    this.users = require("../db/users.json");
  }
  #getDatas(data) {
    if (data === "posts") {
      return this.posts;
    } else if (data === "users") {
      return this.users;
    }
  }

  getAllDatas(data) {
    return this.#getDatas(data);
  }

  getDataById(id, data) {
    const datas = this.#getDatas(data);

    let dataFiltered = datas.filter((data) => data.id == id);

    return dataFiltered;
  }
}

module.exports = Model;
