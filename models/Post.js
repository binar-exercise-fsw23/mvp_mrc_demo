const Data = require("../models/Model");
class Post extends Data {
  constructor() {
    super();
  }

  getAllPosts() {
    return super.getAllDatas("posts");
  }

  getPostById(id) {
    return super.getDataById(id, "posts");
  }
}

module.exports = Post;
