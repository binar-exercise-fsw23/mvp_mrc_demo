const UserModel = require("../models/User");
const UserView = require("../views/userView");
const user = new UserModel();
const userView = new UserView();

class UserController {
  getAllUser(req, res) {
    let data = user.getAllUser();

    // // ini bakal jadi MCR
    res.json({
      message: "Get All User From user view",
      data,
    });

    // ini bakal jadi MVC
    // userView.showUserData(res, {
    //   message: "Get All User From user view",
    //   data,
    // });
  }

  getUserById(req, res) {
    let id = req.params.id;
    let userOne = user.getUserById(id);
    res.json({
      message: `get user by id ${req.params.id}`,
      data: userOne,
    });
  }
}

module.exports = UserController;
